### Zj-Tianditu 天地图

基于天地图开发的VUE组件——Жидзин（Zidjin）系列组件库。



#### 安装

推荐使用 npm 的方式安装。

```shell
npm install zj-tianditu
```



#### 引入

先添加天地图API，在/public/index.html中写入以下内容：

```html
<script src="http://api.tianditu.gov.cn/api?v=4.0&tk=您的密钥" type="text/javascript"></script>
```

全局引入，在 main.js 中写入以下内容：

```js
import Vue from 'vue';
import App from './App.vue';
import ZjTianditu from "zj-tianditu";

Vue.use(ZjTianditu);
new Vue({
  el: '#app',
  render: h => h(App)
});
```

局部引入，在 vue页面文件中写入以下内容

```js
export default {
    components: {
        ZjTianditu: () => import('zj-tianditu'),
    },
};
```



#### 基本用法

初始化中心坐标、缩放比例和背景图层，默认坐标为北京天安门。

![天地图基本用法](https://gitee.com/zhaoxiangming/zj-tianditu/raw/master/images/001.jpg)

[^图像示例]: 注：图像示例统一800×300像素，特殊除外。

```vue
<zj-tianditu :center="{lng: 116.391230, lat: 39.907140}" zoom="17" map-type="HYBRID"></zj-tianditu>

<script>
    export default {
        components: {
            ZjTianditu: () => import('zj-tianditu'),
        },
        data: ()=>({})
    };
</script>
```



#### 标记坐标点

![](https://gitee.com/zhaoxiangming/zj-tianditu/raw/master/images/002.jpg)

```vue
<zj-tianditu :center="center" :markers="markers" ></zj-tianditu>

<script>
    export default {
        components: {
            ZjTianditu: () => import('zj-tianditu'),
        },
        data: ()=>({
            center: {
                /* 中心点为北大图书馆 */
                lng: 116.304470,
                lat: 39.990660,
                zoom: 17, 
            },
            markers: [
                {
                    key: '北京大学',
                    list: [
                        { title: '北京大学图书馆', lng: 116.304470, lat: 39.990660, },
                    ]
                },
            ],
        })
    };
</script>
```



##### 自定义标记坐标点

可根据实际场景自定义图标、尺寸、锚点偏移。

![](https://gitee.com/zhaoxiangming/zj-tianditu/raw/master/images/003.jpg)

```vue
<zj-tianditu :center="center" :markers="markers" ></zj-tianditu>

<script>
    export default {
        components: {
            ZjTianditu: () => import('zj-tianditu'),
        },
        data: ()=>({
            center: {
                /* 中心点为北大图书馆 */
                lng: 116.304470,
                lat: 39.990660,
                zoom: 17, 
            },
            markers: [
               {
                    key: '北京大学',
                    icon_url: require('./icon-local-pku.svg'),
                    icon_size: {w: 32, h: 48},
                    icon_anchor: {w: 16, h: 48},
                    node_title: 'title',
                    list: [
                        { title: '一塔湖图之北大图书馆', lng: 116.304470, lat: 39.990660, },
                    ],
                },
            ],
        })
    };
</script>
```



#### 标签

![](https://gitee.com/zhaoxiangming/zj-tianditu/raw/master/images/004.jpg)

```vue
<zj-tianditu :center="center" :labels="labels" ></zj-tianditu>

<script>
    export default {
        components: {
            ZjTianditu: () => import('zj-tianditu'),
        },
        data: ()=>({
            center: {
                /* 中心点为北大图书馆 */
                lng: 116.304470,
                lat: 39.990660,
                zoom: 17, 
            },
            labels: [
               {
                    key: '北京大学',
                    list: [
                        { label: '北大图书馆', lng: 116.304470, lat: 39.990660, },
                    ],
                },
            ],
        })
    };
</script>
```



##### 自定义标签

![](https://gitee.com/zhaoxiangming/zj-tianditu/raw/master/images/005.jpg)

```vue
<zj-tianditu :center="center" :labels="labels" ></zj-tianditu>

<script>
    export default {
        components: {
            ZjTianditu: () => import('zj-tianditu'),
        },
        data: ()=>({
            center: {
                /* 中心点为北大图书馆 */
                lng: 116.304470,
                lat: 39.990660,
                zoom: 17, 
            },
            labels: [
                {
                    key: '北京大学',
                    offset: {w: 0, h: 0},
                    font_color: '#FFF',
                    background_color: '#D90000',
                    opacity: 0.1, // 不起作用？
                    font_size: 13,
                    border_line: 3,
                    border_color: '#ffaa00',
                    node_label: 'label',
                    list: [
                        { label: '北京大学', lng: 116.304470, lat: 39.990660, tip: '北京市海淀区颐和园南路5号' },
                	],
                },
            ],
        })
    };
</script>
```



#### 多边形

由一组或多组连续的点组成多边形。

![](https://gitee.com/zhaoxiangming/zj-tianditu/raw/master/images/006.jpg)

```vue
<zj-tianditu :center="center" :polygons="polygons" ></zj-tianditu>

<script>
    export default {
        components: {
            ZjTianditu: () => import('zj-tianditu'),
        },
        data: ()=>({
            center: {
                /* 中心点为北大图书馆 */
                lng: 116.304470,
                lat: 39.990660,
                zoom: 14, 
            },
            polygons: [
                {
                key: '北大围墙',
                weight: 5,
                list: [
                    [[116.308900,39.998540],[116.310190,39.984430],[116.299740,39.984120],[116.298370,39.992590],[116.298270,39.996190],[116.300880,39.996410],[116.302740,39.997980],[116.304860,39.998530],],
                ]
            },
            ],
        })
    };
</script>
```



##### 自定义多边形

可根据实际场景自定义边线、填充、镂空等。

![](https://gitee.com/zhaoxiangming/zj-tianditu/raw/master/images/007.jpg)

```vue
<zj-tianditu :center="center" :polygons="polygons" ></zj-tianditu>

<script>
    export default {
        components: {
            ZjTianditu: () => import('zj-tianditu'),
        },
        data: ()=>({
            center: {
                /* 中心点为北大理科一号楼 */
                lng: 116.308610,
                lat: 39.988440,
                zoom: 17, 
            },
            polygons: [
                {
                    key: '理科一号教学楼',
                    color: "#E00", // 边线颜色
                    weight: 3, // 边线宽度
                    opacity: 0.8, // 边的透明度
                    fill_color: "#FFF",
                    fill_opacity: 0.5, // 填充的透明度
                    list: [
                        // 一个图形组，当有多个的时候为镂空
                        [[116.308280,39.988660],[116.308910,39.988690],[116.308950,39.988190],[116.308310,39.988170]],
                        [[116.308440,39.988510],[116.308790,39.988520],[116.308800,39.988350],[116.308460,39.988330]],
                    ]
                },
            ],
        })
    };
</script>
```



#### 气泡窗口

![](https://gitee.com/zhaoxiangming/zj-tianditu/raw/master/images/008.jpg)

```vue
<zj-tianditu :center="center" :bubbles="bubbles" >
    <template v-slot:bubble="slotProps" >{{slotProps.data.title}}</template>
</zj-tianditu>

<script>
    export default {
        components: {
            ZjTianditu: () => import('zj-tianditu'),
        },
        data: ()=>({
            center: {
                /* 中心点为北大图书馆 */
                lng: 116.304470,
                lat: 39.990660,
                zoom: 17, 
            },
            bubbles: [
                {
                    key: '气泡弹窗',
                    size: {w: 100, h: 50},
                    offset: {w: 0, h: -50},
                    list: [
                        { title: '北大图书馆', lng: 116.304470, lat: 39.990660, show: true, },
                    ],
                },
            ],
        })
    };
</script>
```



#### 热力图

![](https://gitee.com/zhaoxiangming/zj-tianditu/raw/master/images/009.jpg)

```vue
<zj-tianditu :center="center" :heat-map="heatMap" map-type="HYBRID" ></zj-tianditu>

<script>
    export default {
        components: {
            ZjTianditu: () => import('zj-tianditu'),
        },
        data: ()=>({
            center: {
                /* 中心点为北大图书馆 */
                lng: 116.304470,
                lat: 39.990660,
                zoom: 16, 
            },
            heatMap: {
                radius: 50,
                max: 300,
                node_name: 'name',
                node_count: 'count',
                list: [
                    { name: '五四体育场', lng: 116.307140, lat: 39.986340, count: 150, },
                    { name: '理科第二教学楼', lng: 116.307520, lat: 39.988240, count: 250, },
                    { name: '斯诺墓前', lng: 116.304300, lat: 39.992740, count: 150, },
                    { name: '未名石', lng: 116.302600, lat: 39.992850, count: 300, },
                    { name: '红四楼', lng: 116.303070, lat: 39.994090, count: 150, },
                    { name: '北岸', lng: 116.303840, lat: 39.994260, count: 30, },
                    { name: '连岛桥', lng: 116.303060, lat: 39.993310, count: 50, },
                    { name: '岛中亭', lng: 116.303570, lat: 39.993370, count: 50, },
                    { name: '岛北侧', lng: 116.303510, lat: 39.993750, count: 50, },
                    { name: '岛南侧', lng: 116.303540, lat: 39.993100, count: 50, },
                    { name: '燕南园', lng: 116.302680, lat: 39.988500, count: 1000, },
                    { name: '红三楼研究生院', lng: 116.302250, lat: 39.994130, count: 200, },
                ]
            },
        })
    };
</script>
```



#### Tianditu Attributes 

| 参数                              | 说明         | 类型          | 可选值                                                       | 默认值 |
| --------------------------------- | ------------ | ------------- | ------------------------------------------------------------ | ------ |
| center                            | 中心坐标     | object        | -                                                            | -      |
| zoom                              | 缩放         | number        | 1-18                                                         | -      |
| map-type                          | 地图类型     | string        | NORMAL 地图,<br />SATELLITE 卫星,<br />HYBRID 卫星混合,<br />TERRAIN 地形<br />TERRAIN_HYBRID 地形混合 | -      |
| bubbles                           | 气泡窗       | array         | -                                                            | -      |
| markers                           | 坐标点       | array<object> | -                                                            | -      |
| labels                            | 标签         | array<object> | -                                                            | -      |
| polygons                          | 多边形       | array         | -                                                            | -      |
| heat-map                          | 热图         | object        | -                                                            | -      |
| clusterer                         | 聚合点       | object        | -                                                            | -      |
| wmsLayers                         | WMS图层      | array<object> | -                                                            | -      |
| images                            | 图像覆盖层   | array<object> | -                                                            | -      |
| enable-click                      | 启动点击     | boolean       | true, 启用click事件                                          | false  |
| (待完成)<br />enable-context-menu | 启动右键菜单 | boolean       | true, 启用右键菜单                                           | false  |



#### Center  Attributes

| 参数 | 说明           | 类型   | 可选值 | 默认值 |
| ---- | -------------- | ------ | ------ | ------ |
| lng  | 经度 Longitude | number | -      | -      |
| lat  | 纬度 Latitude  | number | -      | -      |
| zoom | 缩放，可选     | number | 1-18   | -      |

[^zoom]: 当zoom属性省略时，地图会平移动画到中心坐标。但有zoom时，整个页面会刷新，根据需求小心使用。



#### Markers Attributes

| 参数           | 说明                           | 类型          | 可选值 | 默认值                                                       |
| -------------- | ------------------------------ | ------------- | ------ | ------------------------------------------------------------ |
| key            | 坐标组名称，必须               | number        | -      | -                                                            |
| icon_url       | 坐标点图标                     | url           | -      | -                                                            |
| icon_size      | 图标大小                       | object        | -      | {w: 20, h: 30}                                               |
| icon_anchor    | 图标以左上角为原点的锚点偏移量 | object        | -      | {w: 10, h: 30}                                               |
| list           | 坐标数据列表                   | array<object> | -      | 例：<br />{ title: '天安门', lng: 116.40969, lat: 39.89945, } |
| node_title     | 自定义文字节点                 | string        | -      | title                                                        |
| node_longitude | 自定义经度节点                 | string        | -      | lng                                                          |
| node_latitude  | 自定义纬度节点                 | string        | -      | lat                                                          |

[^icon_anchor]: 注：其偏移量是指以图标的左上角作为原点(0,0)，锚点相对原点的偏移量。例如图标宽20，高30，那么如果希望以图标下边中点为锚点，则应设置为宽向右移10，高下移30。单位px。



#### List of Markers Attributes

| 参数  | 说明     | 类型   | 可选值 | 默认值 |
| ----- | -------- | ------ | ------ | ------ |
| title | 提示文字 | string | -      | -      |
| lng   | 经度     | number | -      | -      |
| lat   | 纬度     | number | -      | -      |



#### Labels Attributes

| 参数             | 说明           | 类型          | 可选值 | 默认值                |
| ---------------- | -------------- | ------------- | ------ | --------------------- |
| key              | 标签组名称     | number        | -      | -                     |
| offset           | 坐标点图标     | url           | -      | -                     |
| font_color       | 文字颜色       | color         | -      | -                     |
| background_color | 背景颜色       | string        | -      | rgba(0,0,0,0.8)       |
| opacity          | 不透明度       | number        | -      | 不起作用？？？        |
| font_size        | 字号           | string        | -      | 12                    |
| border_line      | 边线粗细       | number        | -      | 1                     |
| border_color     | 边线颜色       | color         | -      | rgba(255,255,255,0.5) |
| list             | 坐标数据列表   | array<object> | -      | -                     |
| node_label       | 自定义标签节点 | string        | -      | label                 |
| note_tip         | 自定义提示节点 | string        | -      | tip                   |

[^border_line]: border_line为0时不起作用，需用border_color: 'rgba(255,255,255,0)'



#### List of Labels Attributes

| 参数  | 说明     | 类型   | 可选值 | 默认值 |
| ----- | -------- | ------ | ------ | ------ |
| label | 标签文字 | string | -      | -      |
| lng   | 经度     | number | -      | -      |
| lat   | 纬度     | number | -      | -      |
| tip   | 提示文字 | string | -      | -      |



#### Polygens Attribues

| 参数         | 说明                 | 类型                  | 可选值 | 默认值 |
| ------------ | -------------------- | --------------------- | ------ | ------ |
| key          | 多边形名称           | string                | -      | -      |
| color        | 边线颜色             | color                 | -      | #FFF   |
| weight       | 边线宽度             | number                | -      | 1      |
| opacity      | 边的透明度           | number                | 0-1.0  | 1      |
| fill_color   | 填充的颜色           | color                 | -      | none   |
| fill_opacity | 填充的透明度         | number                | 0-1.0  | 0      |
| list         | 连续的多边形二维列表 | array<array<lng,lat>> | -      | -      |

[^list]: 需要特别说明的是，一个多边形图形是由一组或多组连续的点组成的，但两组点相交时图形镂空。其中，每个点是由经度和纬度组成的数组，如[116.30,39.98]。那么，一个点是长度为2的数组，一组连续的点组成线段，多条线段组成一个多边形，因此，一个多边形实际上是由三维数组构成。



#### List Code of Polygens Attribues

```js
list: [
    // 一个图形组，当有多组的时候为镂空
    [[116.308280,39.988660],[116.308910,39.988690],[116.308950,39.988190],[116.308310,39.988170]],
    [[116.308440,39.988510],[116.308790,39.988520],[116.308800,39.988350],[116.308460,39.988330]],
]
```



#### Bubbles Attribues

| 参数   | 说明             | 类型          | 可选值      | 默认值           |
| ------ | ---------------- | ------------- | ----------- | ---------------- |
| key    | 气泡窗名称       | string        | -           | -                |
| size   | 气泡窗尺寸       | object        | -           | {w: 180, h: 180} |
| offset | 气泡窗左上角偏离 | object        |             | {w: 0, h: 0}     |
| sytle  | 窗口样式         | string        | dark/bright | bright           |
| slot   | 指定槽名称       |               |             | bubble           |
| list   | 气泡窗列表       | array<object> | -           | -                |

[^slot]: 气泡窗口默认样式名：class="bubble-area bubble"，自定义样式则为class="bubble-area slotName", id为id="bubble_i_j"，可通过此方法对样式进行覆盖CSS或定位标签



#### List of Bubbles Attribues

| 参数 | 说明       | 类型    | 可选值 | 默认值 |
| ---- | ---------- | ------- | ------ | ------ |
| lng  | 经度       |         |        |        |
| lat  | 纬度       |         |        |        |
| show | 显示气泡窗 | boolean | true   | false  |



#### Heat-Map Attribues

| 参数       | 说明                 | 类型          | 可选值 | 默认值 |
| ---------- | -------------------- | ------------- | ------ | ------ |
| radius     | 热点半径             | number        | -      | 100    |
| max        | 最大值，最大值为红色 | number        | -      | 300    |
| node_count | 自定义数量节点       | string        | -      | count  |
| list       | 热点图列表           | array<object> | -      | -      |

[^]: 热图只能是一张



#### List of Heat-Map Attribues

| 参数  | 说明 | 类型   | 可选值 | 默认值 |
| ----- | ---- | ------ | ------ | ------ |
| lng   | 经度 | number | -      | -      |
| lat   | 纬度 | number | -      | -      |
| count | 数量 | number | -      | -      |





#### Clusterer Attributes

| 参数           | 说明                           | 类型          | 可选值 | 默认值                                                       |
| -------------- | ------------------------------ | ------------- | ------ | ------------------------------------------------------------ |
| icon_url       | 坐标点图标                     | url           | -      | -                                                            |
| icon_size      | 图标大小                       | object        | -      | {w: 20, h: 30}                                               |
| icon_anchor    | 图标以左上角为原点的锚点偏移量 | object        | -      | {w: 10, h: 30}                                               |
| list           | 坐标数据列表                   | array<object> | -      | 例：<br />{ title: '天安门', lng: 116.40969, lat: 39.89945, } |
| node_title     | 自定义文字节点                 | string        | -      | title                                                        |
| node_longitude | 自定义经度节点                 | string        | -      | lng                                                          |
| node_latitude  | 自定义纬度节点                 | string        | -      | lat                                                          |

[^注]: Clusterer聚合图和Markers坐标点的配置是一模一样的，维一的区别是Markers是带分组的，而Clusterer是一个单组数据对象。
[^icon_anchor]: 注：其偏移量是指以图标的左上角作为原点(0,0)，锚点相对原点的偏移量。例如图标宽20，高30，那么如果希望以图标下边中点为锚点，则应设置为宽向右移10，高下移30。单位px。



#### List of Clusterer Attributes

| 参数  | 说明     | 类型   | 可选值 | 默认值 |
| ----- | -------- | ------ | ------ | ------ |
| title | 提示文字 | string | -      | -      |
| lng   | 经度     | number | -      | -      |
| lat   | 纬度     | number | -      | -      |



#### Wms-Layers Attributes

| 参数        | 说明                              | 类型    | 可选值      | 默认值        |
| ----------- | --------------------------------- | ------- | ----------- | ------------- |
| key         | 坐标点图标，必须                  | string  | -           | -             |
| url         | 图层服务地址                      | url     | -           | -             |
| version     | 请求服务的版本                    | string  | -           | "1.1.1"       |
| layers      | 用","分隔的多个图层列表。         | string  | -           | "0,1,2,3"     |
| transparent | 输出图像背景是否透明              | boolean | true        | false         |
| styles      | 每个请求图层的用","分隔的描述样式 | string  | -           | -             |
| srs         | 地图投影类型                      | string  | "EPSG:4326" | "EPSG:900913" |
| format      | 输出图像的类型                    | string  | "image/png" | "image/jpeg"  |
|             |                                   |         |             |               |



#### Images Attributes

| 参数 | 说明           | 类型          | 可选值 | 默认值 |
| ---- | -------------- | ------------- | ------ | ------ |
| key  | 图像组名称     | string        | -      | -      |
| list | 图像覆盖物列表 | array<object> | -      | -      |

#### List of Images Attributes

| 参数 | 说明             | 类型   | 可选值    | 默认值 |
| ---- | ---------------- | ------ | --------- | ------ |
| url  | 图片的地址       | string | -         | -      |
| sw   | 矩形区域的西南角 | object | {lng,lat} | -      |
| ne   | 矩形区域的东北角 | object | {lng,lat} | -      |
|      |                  |        |           |        |



#### Tianditu Events 

| 事件名称 | 说明                                 | 回调参数                                             |
| -------- | ------------------------------------ | ---------------------------------------------------- |
| marker   | 当点击坐标时触发                     | key: 组键名, <br />config: 组配置, <br />各字段属性, |
| label    | 当点击标签时触发                     | key: 组键名, <br />config: 组配置, <br />各字段信息  |
| polygen  | 当点击多边形时触发                   | key: 组键名, <br />config: 组配置, <br />各字段信息  |
| click    | 点击地图时触发，需enable-click为true | {containerPoint, <br/>layerPoint, <br/>lng, lat}     |
| move     | 拖动地图时触发                       | {lng, lat}                                           |
| zoom     | 缩放地图时触发                       | 1-18                                                 |
| resize   | 窗口尺寸变化时触发                   | 天地图原生event                                      |



#### Tianditu Methods

| 方法名       | 说明                           | 参数           |
| ------------ | ------------------------------ | -------------- |
| resize       | 通知地图其容器大小已更改       | -              |
| autoViewport | 根据坐标点数组自动设置地图视野 | array<lng,lat> |
|              |                                |                |





#### 版本说明

**V1.0.61.211028-release** 

修正若干地图调用时不稳定的问题，早期稳定版，用于基本的地图数据展示，无鼠标交互事件等。已停止更新。

**V1.1.0.211028-alpha**

添加天地图原生坐标标记事件@marker、多边形事件@polygen、标签事件@label。

**V1.1.1.211029-beta**

支持天地图原生拖动事件@move、缩放事件@zoom。

**V1.1.3.2.211031-release**

修正天地图事件部分不良的问题，修正数据导入时未检查list数组不合法的问题。

**V1.1.4.211108-beta**

支持天地图原生的数据聚合功能。

**V1.1.6.211120-beta**

支持天地图WMS图层功能，支持地图点击事件@click。

**V1.1.7.211121-release**

修正天地图WMS图层key重复时没有删除原图层的问题。







