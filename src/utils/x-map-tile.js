
const XMapTile = function() {};

XMapTile.toTile = function(lng, lat, zoom) {
    let resolution = {
        18: 5.36441802978515E-06,
        17: 1.07288360595703E-05,
        16: 2.1457672119140625E-05,
        15: 4.29153442382814E-05,
        14: 8.58306884765629E-05,
        13: 0.000171661376953125,
        12: 0.00034332275390625,
        11: 0.0006866455078125,
        10: 0.001373291015625,
        9: 0.00274658203125,
        8: 0.0054931640625,
        7: 0.010986328125,
        6: 0.02197265625,
        5: 0.0439453125,
        4: 0.087890625,
        3: 0.17578125,
        2: 0.3515625,
        1: 0.703125,
    };
    // 矩形框坐标
    let x = Math.floor((lng + 180.0) / (resolution[zoom] * 256));
    let y = Math.floor((90.0 - lat) / (resolution[zoom] * 256 / 2));
    let result = {x: x, y: y};
    return result;
};

XMapTile.toLngLat = function(x, y, zoom) {
    let resolution = {
        18: 5.36441802978515E-06,
        17: 1.07288360595703E-05,
        16: 2.1457672119140625E-05,
        15: 4.29153442382814E-05,
        14: 8.58306884765629E-05,
        13: 0.000171661376953125,
        12: 0.00034332275390625,
        11: 0.0006866455078125,
        10: 0.001373291015625,
        9: 0.00274658203125,
        8: 0.0054931640625,
        7: 0.010986328125,
        6: 0.02197265625,
        5: 0.0439453125,
        4: 0.087890625,
        3: 0.17578125,
        2: 0.3515625,
        1: 0.703125,
    };
    // let lng = Math.floor((resolution[zoom] * 256) * x) - 180,
    // let lat = 90 - Math.floor((resolution[zoom] * 256) * y),

    let result = {
        nw: {
            lng: Math.floor((resolution[zoom] * 256) * x) - 180,
            lat: 90 - Math.floor((resolution[zoom] * 256 / 2) * y),
        },
        ne: {
            lng: Math.floor((resolution[zoom] * 256) * (x + 1)) - 180,
            lat: 90 - Math.floor((resolution[zoom] * 256 / 2) * y),
        },
        sw: {
            lng: Math.floor((resolution[zoom] * 256) * x) - 180,
            lat: 90 - Math.floor((resolution[zoom] * 256 / 2) * (y + 1)),
        },
        se: {
            lng: Math.floor((resolution[zoom] * 256) * (x + 1)) - 180,
            lat: 90 - Math.floor((resolution[zoom] * 256 / 2) * (y + 1)),
        },
    };
    // let result = {lng: lng, lat: lat};
    return result;

};

export default XMapTile;