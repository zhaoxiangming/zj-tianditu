/** 
 * @brief 天地图FIF默认配置文件。
 * 
 * @author Ian.Sieyoo.Zhao       
 * @email ian@pku.edu.cn         
 * @version 1.0.1.200808alpha   
 * @date 2021-06-15             
 * @license GNU General Public License (GPL)  //软件许可协议
 * 
 */
            
const config = {
    datas: [
        [],
        [],
    ],
}

export default config;